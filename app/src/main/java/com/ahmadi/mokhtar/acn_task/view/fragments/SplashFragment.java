package com.ahmadi.mokhtar.acn_task.view.fragments;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavOptions;
import androidx.navigation.Navigation;
import android.view.View;
import com.ahmadi.mokhtar.acn_task.R;
import com.ahmadi.mokhtar.acn_task.view.base.BaseFragment;
import java.util.concurrent.TimeUnit;
import io.reactivex.Completable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.schedulers.Schedulers;



public class SplashFragment extends BaseFragment {

Disposable disposable =null;

    @Override
    protected int layoutRes() {
        return R.layout.fragment_splash;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        disposable = Completable.complete().delay(2,TimeUnit.SECONDS, Schedulers.io())
                .subscribeWith(new DisposableCompletableObserver() {
                    @Override
                    public void onComplete() {
                        Navigation.findNavController(view).navigate(R.id.action_splashFragment_to_mainFragment ,null,
                                new NavOptions.Builder()
                                        .setPopUpTo(R.id.splashFragment,
                                                true).build());
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        disposable.dispose();
    }
}
