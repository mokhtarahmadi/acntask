package com.ahmadi.mokhtar.acn_task.viewModel;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.ahmadi.mokhtar.acn_task.App;
import com.ahmadi.mokhtar.acn_task.di.DIComponent;


public class ViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    private App app;
    public ViewModelFactory(App app) {
        this.app = app;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        T t = super.create(modelClass);

        if (t instanceof DIComponent.Injectable){
            ((DIComponent.Injectable) t).inject(app.di);
        }

        return t;
    }
}
