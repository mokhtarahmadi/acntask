package com.ahmadi.mokhtar.acn_task.data.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import com.ahmadi.mokhtar.acn_task.data.database.converter.Converter;
import com.ahmadi.mokhtar.acn_task.data.database.movieRepository.Movies;
import com.ahmadi.mokhtar.acn_task.data.database.movieRepository.MoviesDao;


@Database(entities = {Movies.class}, version = 1)
@TypeConverters({Converter.class})
public abstract class AppDataBase extends RoomDatabase {
   public abstract MoviesDao getMoviesDao();
}
