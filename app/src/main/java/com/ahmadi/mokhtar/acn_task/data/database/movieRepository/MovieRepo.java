package com.ahmadi.mokhtar.acn_task.data.database.movieRepository;

import java.util.List;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

public interface MovieRepo {

    Completable insert(Movies movies);
    Completable deleteAll();
    Single<Movies> movieById(int id);
    Observable<List<Movies>> getAllMovies();
}
