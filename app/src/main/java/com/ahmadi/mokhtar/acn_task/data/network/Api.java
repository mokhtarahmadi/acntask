package com.ahmadi.mokhtar.acn_task.data.network;

import com.ahmadi.mokhtar.acn_task.data.database.movieRepository.Movies;

import java.util.List;
import io.reactivex.Observable;
import retrofit2.http.GET;

public interface Api {
    @GET("json/movies.json")
    Observable<List<Movies>> getSearchData();
}
