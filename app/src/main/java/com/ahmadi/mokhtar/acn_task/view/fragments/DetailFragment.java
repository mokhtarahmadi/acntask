package com.ahmadi.mokhtar.acn_task.view.fragments;


import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toolbar;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import com.ahmadi.mokhtar.acn_task.R;
import com.ahmadi.mokhtar.acn_task.data.database.movieRepository.Movies;
import com.ahmadi.mokhtar.acn_task.view.base.BaseFragment;
import com.bumptech.glide.Glide;

import butterknife.BindView;


public class DetailFragment extends BaseFragment {

    @BindView(R.id.movieGenre)
    TextView movieGenre;

    @BindView(R.id.movieRate)
    TextView movieRate;

    @BindView(R.id.movieYear)
    TextView movieYear;

    @BindView(R.id.movieTitle)
    TextView movieTitle;

    @BindView(R.id.movieImage)
    ImageView movieImage;


    @Override
    protected int layoutRes() {
        return R.layout.fragment_detail;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Movies movies = (Movies) getArguments().getSerializable("movie");

        String url = movies.getImage();
        Glide.with(this)
                .load(url)
                .centerCrop()
                .placeholder(R.drawable.ic_launcher_background)
                .into(movieImage);

        movieTitle.setText(movies.getTitle());
        movieRate.setText(movies.getRating()+"");
        movieYear.setText(movies.getReleaseYear()+"");

        StringBuilder sb = new StringBuilder();
        int i =0 ;
        for (String str : movies.getGenre())
        {
            i++;
            sb.append(str.toString());
            if (i!=movies.getGenre().size())
            {
                sb.append(",");
            }
        }
        movieGenre.setText(sb);
    }


}
