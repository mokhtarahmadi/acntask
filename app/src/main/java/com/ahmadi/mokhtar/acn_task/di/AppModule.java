package com.ahmadi.mokhtar.acn_task.di;

import android.app.Application;
import android.content.Context;
import android.provider.SyncStateContract;

import androidx.room.Room;

import com.ahmadi.mokhtar.acn_task.App;
import com.ahmadi.mokhtar.acn_task.data.database.AppDataBase;
import com.ahmadi.mokhtar.acn_task.data.database.movieRepository.MovieRepo;
import com.ahmadi.mokhtar.acn_task.data.database.movieRepository.MovieRepository;
import com.ahmadi.mokhtar.acn_task.data.database.movieRepository.MoviesDao;
import com.ahmadi.mokhtar.acn_task.data.network.Api;
import com.ahmadi.mokhtar.acn_task.utils.AppConstants;
import com.ahmadi.mokhtar.acn_task.viewModel.MovieViewModel;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private final App mApplication;

    public AppModule(App application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return mApplication.getApplicationContext();
    }

    @Provides
    App provideApplication() {
        return mApplication;
    }


    @Singleton
    @Provides
    AppDataBase provideDatabase(Context context)
    {
        return Room.databaseBuilder(context, AppDataBase.class, AppConstants.DB_NAME).build();
    }



    @Singleton
    @Provides
    MoviesDao providesMoviesAppDataBaseDao(AppDataBase database) {
        return database.getMoviesDao();
    }

    @Singleton
    @Provides
    MovieRepository provideMovieRepository(Context context , AppDataBase dataBase , Api api){
        return new MovieRepository(context,dataBase,api);
    }

    @Singleton
    @Provides
    MovieRepo provideMovieRepo(MovieRepository movieRepository){
        return movieRepository;
    }




}
