package com.ahmadi.mokhtar.acn_task.data.database.movieRepository;

import android.content.Context;
import com.ahmadi.mokhtar.acn_task.data.database.AppDataBase;
import com.ahmadi.mokhtar.acn_task.data.network.Api;
import com.ahmadi.mokhtar.acn_task.utils.CommonUtils;
import java.util.List;
import javax.inject.Inject;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.Single;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class MovieRepository implements MovieRepo {
    private Context context;
    private AppDataBase appDataBase;
    private Api api;

    @Inject
    public MovieRepository(Context context , AppDataBase appDataBase , Api api) {
        this.context = context;
        this.appDataBase = appDataBase;
        this.api = api;
    }

    @Override
    public Completable insert(Movies movies) {
        return null;
    }

    @Override
    public Completable deleteAll() {
        return null;
    }

    @Override
    public Single<Movies> movieById(int id) {
        return null;
    }

    @Override
    public Observable<List<Movies>> getAllMovies() {
        Observable<List<Movies>> listObservable = null;
        if (CommonUtils.isNetworkConnected(context)){
            listObservable = getNewsFromNet() ;
        }

        if (CommonUtils.isNetworkConnected(context)){
            return Observable.concatArrayEager(listObservable,getNewsFromDb());
        }else {
            return getNewsFromDb();
        }

    }



    private Observable<List<Movies>> getNewsFromDb(){
        return appDataBase.getMoviesDao().getAllMovies();
    }

    private Observable<List<Movies>> getNewsFromNet(){
        return api.getSearchData().map(new Function<List<Movies>, List<Movies>>() {
            @Override
            public List<Movies> apply(List<Movies> movies) throws Exception {
                for (Movies item : movies){
                    if (item!=null)
                        appDataBase.getMoviesDao().insert(item);
                }
                return movies;
            }
        });
    }
}
