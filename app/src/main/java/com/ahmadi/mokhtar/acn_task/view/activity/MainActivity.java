package com.ahmadi.mokhtar.acn_task.view.activity;


import android.os.Bundle;
import androidx.navigation.Navigation;
import com.ahmadi.mokhtar.acn_task.R;
import com.ahmadi.mokhtar.acn_task.di.DIComponent;
import com.ahmadi.mokhtar.acn_task.view.base.BaseActivity;
import com.ahmadi.mokhtar.acn_task.viewModel.MovieViewModel;

public class MainActivity extends BaseActivity implements DIComponent.Injectable {


    private MovieViewModel viewModel;



    @Override
    protected int layoutRes() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Navigation.findNavController(this,R.id.mainNav);



    }

    @Override
    public void inject(DIComponent diComponent) {
        diComponent.inject(this);
    }
}
