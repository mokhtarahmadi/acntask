package com.ahmadi.mokhtar.acn_task.viewModel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.ahmadi.mokhtar.acn_task.data.database.movieRepository.MovieRepo;
import com.ahmadi.mokhtar.acn_task.data.database.movieRepository.Movies;
import com.ahmadi.mokhtar.acn_task.di.DIComponent;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class MovieViewModel extends ViewModel implements DIComponent.Injectable {

    public MutableLiveData<List<Movies>> movieResult  =new MutableLiveData();
    public MutableLiveData<String>  movieError =new  MutableLiveData();
    public MutableLiveData<Boolean> movieLoader =new MutableLiveData();

    @Inject
    MovieRepo movieRepo;



    public void getMovie(){
        DisposableObserver<List<Movies>> observer = new DisposableObserver<List<Movies>>() {
            @Override
            public void onNext(List<Movies> movies) {
                movieResult.postValue(movies);
                movieLoader.postValue(false);
            }


            @Override
            public void onError(Throwable e) {
                movieError.postValue(e.getMessage());
                movieLoader.postValue(false);
            }

            @Override
            public void onComplete() {

            }
        };


        movieRepo.getAllMovies().subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .debounce(400, TimeUnit.MILLISECONDS)
                .subscribe(observer);
    }


    @Override
    public void inject(DIComponent diComponent) {
        diComponent.inject(this);
    }
}
