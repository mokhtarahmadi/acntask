package com.ahmadi.mokhtar.acn_task;

import android.app.Application;

import com.ahmadi.mokhtar.acn_task.di.ApiModule;
import com.ahmadi.mokhtar.acn_task.di.AppModule;
import com.ahmadi.mokhtar.acn_task.di.DIComponent;
import com.ahmadi.mokhtar.acn_task.di.DaggerDIComponent;


public class App extends Application {

    public DIComponent di;
    @Override
    public void onCreate() {
        super.onCreate();

        di = DaggerDIComponent
                .builder()
                .apiModule(new ApiModule())
                .appModule(new AppModule(this))
                .build();



    }


}
