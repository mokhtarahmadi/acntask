package com.ahmadi.mokhtar.acn_task.di;

import androidx.annotation.Keep;
import com.ahmadi.mokhtar.acn_task.view.activity.MainActivity;
import com.ahmadi.mokhtar.acn_task.viewModel.MovieViewModel;
import javax.inject.Singleton;
import dagger.Component;


@Keep
@Singleton
@Component(modules = {AppModule.class, ApiModule.class})
public interface DIComponent  {

    interface Injectable {
        void inject(DIComponent diComponent);
    }

    void inject(MainActivity mainActivity);
    void inject(MovieViewModel movieViewModel);
}
