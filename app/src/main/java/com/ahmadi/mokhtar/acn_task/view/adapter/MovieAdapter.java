package com.ahmadi.mokhtar.acn_task.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.ahmadi.mokhtar.acn_task.R;
import com.ahmadi.mokhtar.acn_task.data.database.movieRepository.Movies;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class MovieAdapter  extends RecyclerView.Adapter<MovieAdapter.MyViewHolder>{
    private List<Movies>  myList;
    private List<Movies> moviesListFilter;
    private Context mContext;
    public CallBack callBack;


    public MovieAdapter(List<Movies> myList, Context mContext , CallBack callBack ) {
        this.myList = myList;
        this.moviesListFilter = myList;
        this.mContext = mContext;
        this.callBack = callBack;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.movieTitle.setText(myList.get(position).getTitle());
        String url = myList.get(position).getImage();
        Glide.with(mContext)
            .load(url)
            .centerCrop()
            .placeholder(R.drawable.ic_launcher_background)
            .into(holder.movieImage);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBack.setItem(myList.get(position));
            }
        });

    }

    @Override
    public int getItemCount() {
        return myList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView movieImage;
        TextView movieTitle;

        public MyViewHolder(View view) {
            super(view);

            movieImage = view.findViewById(R.id.movieImage);
            movieTitle = view.findViewById(R.id.movieTitle);

        }
    }

    public interface  CallBack{
        void setItem(Movies movies);
    }


}
