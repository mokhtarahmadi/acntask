package com.ahmadi.mokhtar.acn_task.data.database.movieRepository;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import java.util.List;

import io.reactivex.Observable;

@Dao
public interface MoviesDao {

    @Insert
    void insert(Movies movies);

    @Query("Delete from movies")
    void deleteAll();

    @Query("SELECT * FROM movies WHERE moviesId = :id")
    Movies moviesById(int id) ;

    @Query("SELECT * from movies ORDER BY moviesId ASC")
    Observable<List<Movies>> getAllMovies();

}
