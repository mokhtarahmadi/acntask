package com.ahmadi.mokhtar.acn_task.view.fragments;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;

import com.ahmadi.mokhtar.acn_task.App;
import com.ahmadi.mokhtar.acn_task.R;
import com.ahmadi.mokhtar.acn_task.data.database.movieRepository.Movies;
import com.ahmadi.mokhtar.acn_task.view.adapter.MovieAdapter;
import com.ahmadi.mokhtar.acn_task.view.base.BaseFragment;
import com.ahmadi.mokhtar.acn_task.viewModel.MovieViewModel;
import com.ahmadi.mokhtar.acn_task.viewModel.ViewModelFactory;
import java.util.List;
import butterknife.BindView;


public class MainFragment extends BaseFragment implements MovieAdapter.CallBack {


    private MovieViewModel viewModel;
    private List<Movies> moviesList;
    private MovieAdapter adapter;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    View view;

    @Override
    protected int layoutRes() {
        return R.layout.fragment_main;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.view = view;
        viewModel = ViewModelProviders.of(this,new ViewModelFactory((App) getActivity().getApplication())).get(MovieViewModel.class);
        viewModel.getMovie();
        initObserver();

    }

    private void setUpRecyclerMovieAdapter() {
        adapter = new MovieAdapter(moviesList, getActivity(),this);
        recyclerView.setAdapter(adapter);

        @SuppressLint("WrongConstant") final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(
                getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
    }



    void initObserver(){
        viewModel.movieResult.observe(this, new Observer<List<Movies>>() {
            @Override
            public void onChanged(List<Movies> movies) {
                moviesList = movies;
                if (moviesList!=null)
                  setUpRecyclerMovieAdapter();

            }
        });

        viewModel.movieLoader.observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {

            }
        });

        viewModel.movieError.observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {

            }
        });
    }


    @Override
    public void setItem(Movies movies) {

        Bundle bundle = new Bundle();
        bundle.putSerializable("movie",movies);
        Navigation.findNavController(view).navigate(R.id.action_mainFragment_to_detailFragment,bundle, null,null);
    }


}
